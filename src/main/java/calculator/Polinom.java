package calculator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Polinom  {

    private ArrayList<Monom> monoame;
    public Polinom()
    {
        super();
        monoame=new ArrayList<Monom>();
    }

    public ArrayList<Monom> getMonoame()
    {
        return monoame;
    }

    public void adauga(Monom m)
    {
        this.monoame.add(m);
    }


    public void sortPolinom(Polinom p)
    {
        Collections.sort(p.monoame);
    }


    public boolean validarePolinom(String polinom) {
        boolean ok = false;
        String afisare = "[\\dx\\^\\d|x\\^\\d|\\d|x|\\-\\dx\\^\\d|\\-x\\^\\d|\\-\\d|\\-x]";
        Pattern pat = Pattern.compile(afisare);
        Matcher mat = pat.matcher(polinom);
        ok = mat.find();
        return ok;
    }



    public Polinom descompunePolinom(String polinom)
    {
        String[] monoms = null;
        ArrayList<Monom> monomul = new ArrayList<Monom>();
        monoms = polinom.split("(?=-)|(?=\\+)");
        for(String str: monoms)
        {
            System.out.println(str);
            Monom mon = new Monom();
            monomul.add(mon.descompuneMonom(str));
        }
        this.monoame = monomul;
        System.out.println(this.monoame.toString());
        return this;
    }


    public String afisarePolinom() {
        System.out.println(this.monoame.toString());
        String afisRezultat = "";
        if (this.monoame.isEmpty())
            return afisRezultat = "0";
        afisRezultat = this.monoame.get(0).toString();
        for (int i = 1; i < this.monoame.size(); i++)
        {
            if (this.monoame.get(i).getExp() > 1)
                afisRezultat += "+" + this.monoame.get(i).toString();
            else if (this.monoame.get(i).getExp() == 1)
                if (this.monoame.get(i).getCoef() > 0)
                    if (this.monoame.get(i).getCoef() == 1)
                        afisRezultat += "+x";
                    else
                        afisRezultat += "+" + this.monoame.get(i).getCoef() + "x";
                else if (this.monoame.get(i).getCoef() == -1)
                    afisRezultat += "-x";
                else
                    afisRezultat += this.monoame.get(i).getCoef() + "x";
            else if (this.monoame.get(i).getExp() == 0)
                if (this.monoame.get(i).getCoef() > 0)
                    afisRezultat += "+" + this.monoame.get(i).getCoef();
                else if (this.monoame.get(i).getCoef() == -1 && this.monoame.get(i).getExp() == 1)
                    afisRezultat += "-x";
                else
                    afisRezultat += this.monoame.get(i).toString();


        }
        return afisRezultat;
    }

    public Polinom adunarePolinoame(Polinom p1, Polinom p2)
    {
        Polinom RezFinal = new Polinom();

        for( Monom m1 : p1.getMonoame())
        {
            boolean OK = false;
            for(Monom m2 : p2.getMonoame())
                if(m1.getExp() == m2.getExp())
                {
                    OK = true;
                    if(m1.getCoef() + m2.getCoef() != 0)
                    {
                        Monom rezultat = new Monom(m1.getCoef() + m2.getCoef(), m1.getExp());
                        RezFinal.monoame.add(rezultat);
                    }

                }
            if(OK == false)
                RezFinal.monoame.add(m1);   // adaug polinomle pentru care nu a fost gasit corespondent din primul polinom
        }
        for(Monom m2 : p2.getMonoame())
        {
            boolean OK1 = false;
            for( Monom m1 : p1.getMonoame())
                if(m2.getExp() == m1.getExp())
                    OK1 = true;
            if( OK1 == false)
                RezFinal.monoame.add(m2); // adapolinomle pentru care nu a fost gasit corespondent din cel de al doilea polinom
        }
        RezFinal.sortPolinom(RezFinal);
        this.monoame = RezFinal.monoame;
        return this;
    }




    public Polinom scaderePolinoame(Polinom p1, Polinom p2)
    {
        Polinom RezFinal = new Polinom();
        for( Monom m1 : p1.getMonoame())
        {
            boolean OK = false;
            for(Monom m2 : p2.getMonoame())
                if(m1.getExp() == m2.getExp())
                {
                    OK = true;
                    if(m1.getCoef() + m2.getCoef() != 0)
                    {
                        Monom rezultat = new Monom(m1.getCoef() -m2.getCoef(), m1.getExp());
                        RezFinal.adauga(rezultat);
                    }

                }
            if(OK == false)
                RezFinal.monoame.add(m1);   // adapolinomle pentru care nu a fost gasit corespondent din primul polinom
        }
        for(Monom m2 : p2.getMonoame())
        {
            boolean OK1 = false;
            for( Monom m1 : p1.getMonoame())
                if(m2.getExp() == m1.getExp())
                    OK1 = true;
            if( OK1 == false)
            {
                m2.setCoef(-m2.getCoef());
                RezFinal.monoame.add(m2); // adapolinomle pentru care nu a fost gasit corespondent din cel de al doilea polinom
            }}

        RezFinal.sortPolinom(RezFinal);
        this.monoame =  RezFinal.monoame;
        return RezFinal;
    }

    public Polinom inmultirePolinoame(Polinom p1, Polinom p2)
    {
        Polinom RezFinal = new Polinom();
        for(Monom m1: p1.getMonoame())
            for(Monom m2: p2.getMonoame())
            {
                Monom resultMonom = new Monom(m1.getCoef() * m2.getCoef(),m1.getExp() + m2.getExp());
                RezFinal.monoame.add(resultMonom);
            }
        RezFinal.sortPolinom(RezFinal);
        this.monoame = RezFinal.monoame;
        return this;
    }

    public Polinom derivarePolinoame(Polinom p)
    {
        Polinom RezFinal = new Polinom();
        for(Monom m: p.getMonoame())
        {
            if (m.getExp() == 0)
                continue;
            else if(m.getExp() == 1)
                m.setExp(0);
            else
            {
                m.setCoef(m.getCoef() * m.getExp());
                m.setExp(m.getExp() - 1);
            }

            RezFinal.monoame.add(m);
        }
        RezFinal.sortPolinom(RezFinal);
        this.monoame = RezFinal.monoame;
        return this;
    }

    public String integrarePolinoame (Polinom p)
    {
        String RezFinal = "";
        for(Monom m: p.monoame){
            if(m.getExp() == 0)
            {
                String str = Integer.toString(m.getCoef());
                if(m.getCoef() > 0 && m != p.monoame.get(0))
                    RezFinal += "+" + str + "x";
                else
                    RezFinal += str + "x";
            }
            else
            {
                String str1 = "";
                String str = Integer.toString(m.getExp() + 1);
                if(m.getCoef() % (m.getExp() + 1) == 0)
                {
                    str1 = Integer.toString(m.getCoef() / (m.getExp() + 1));
                    if(m.getCoef() > 0 && m != p.monoame.get(0))
                        RezFinal += "+" + str1 + "x^" + str;
                    else
                        RezFinal += str1 + "x^" + str;
                }
                else
                {
                    str1 = Integer.toString(m.getCoef());
                    if(m.getCoef() > 0 && m != p.monoame.get(0))
                        RezFinal += "+"+str1+"/"+str+"x^"+str;
                    else
                        RezFinal += str1+"/"+str+"x^"+str;
                }
            }
        }
        return RezFinal;
    }

    @Override
    public String toString() {
        return "polinoame.Polinom [monoame=" + monoame + "]";
    }
}
