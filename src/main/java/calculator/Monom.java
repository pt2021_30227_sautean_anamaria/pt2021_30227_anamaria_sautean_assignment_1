package calculator;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Monom implements Comparable<Monom>{

    private int coef;   //coeficientul monomului
    private int exp;    // exponentul monomului

    public Monom(int coef, int exp) {

        super();
        this.coef = coef;
        this.exp = exp;

    }

    public Monom() {

    }


    public int getCoef() {
        return coef;
    }

    public void setCoef(int coef) {
        this.coef = coef;
    }

    public int getExp() {
        return exp;
    }

    public void setExp (int exp) {
        this.exp = exp;
    }

    public Monom descompuneMonom(String monom)
    {
        if (monom.contains("^") && monom.contains("x"))  // monomul are exponentul >1
        {
            String[] element = monom.split("\\^");
            int i = Integer.parseInt(element[1]);
            this.exp = i;
        }
        if (!monom.contains("x"))
            this.exp = 0;      // exponentul monomului este 0 rezulta ca avem termen liber
        if (monom.charAt(0) == 120)    // 120 este codul ASCII pentru x
        {
            this.coef = 1;
        }
        if (monom.charAt(0) == 45 && monom.charAt(1) == 120)   // 45 este codul ASCII pnetru "-"  deci elementul este de forma  -x
        {
            this.coef = -1;
        }
        else if (monom.charAt(0) != 120 && monom.contains("x") )
        {
            int i = 0;
            String[] elem = monom.split("x");
            if (monom.charAt(0) == 43 && monom.charAt(1) == 120)  // elementul e de forma +x,  43 fiind codul ASCII pentru "+"
                this.coef = 1;
            else if (monom.charAt(0) == 45 || monom.charAt(1) == 43)  // +-
            {
                i = Integer.parseInt(elem[0]);
                this.coef = i;
            }
            else
                this.coef = Integer.parseInt(elem[0]);
        } else {
            int i = Integer.parseInt(monom);
            this.coef = i;
        }
        // System.out.println(this.getCoef());
        // System.out.println(this.getExp());
        return this;
    }

    @Override
    public String toString() {
        return coef + "x^" + exp;
    }
    public int compareTo(Monom m) {
        return m.getExp() - this.exp;
    }


}

