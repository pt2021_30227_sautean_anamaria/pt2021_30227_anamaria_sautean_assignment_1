package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import calculator.Monom;
import calculator.Polinom;

public class Controller {
    private View viewOne;

    public Controller(View viewOne) {
        this.viewOne = viewOne;
        this.viewOne.addPolyCalcListener(new PolyCalcListener());
    }

    class PolyCalcListener implements ActionListener {
        String p1;
        String p2;
        Polinom polinom1 = new Polinom();
        Polinom polinom2 = new Polinom();
        Polinom result = new Polinom();

        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == viewOne.getSuma()) {
                boolean ok1, ok2;
                p1 = viewOne.getPrimulPolinom();
                p2 = viewOne.getAlDoileaPolinom();
                ok1 = polinom1.validarePolinom(p1);
                ok2 = polinom2.validarePolinom(p2);
                System.out.println(viewOne.getPrimulPolinom());
                System.out.println(viewOne.getAlDoileaPolinom());
                if (ok1 == false || ok2 == false)
                    viewOne.showError("Invalid data/ Date invalide");
                else {
                    polinom1.descompunePolinom(p1);
                    polinom2.descompunePolinom(p2);
                    System.out.println(polinom1.toString());
                    System.out.println(polinom2.toString());
                    result.adunarePolinoame(polinom1, polinom2);
                    System.out.println(result.toString());
                    viewOne.result.setText(result.afisarePolinom());
                }
            }

            if (e.getSource() == viewOne.getScadere()) {
                boolean ok3, ok4;
                p1 = viewOne.getPrimulPolinom();
                p2 = viewOne.getAlDoileaPolinom();
                ok3 = polinom1.validarePolinom(p1);
                ok4 = polinom2.validarePolinom(p2);
                if (ok3 == false || ok4 == false)
                    viewOne.showError("Invalid data/ Date invalide");
                else {
                    polinom1.descompunePolinom(p1);
                    polinom2.descompunePolinom(p2);
                    result.scaderePolinoame(polinom1, polinom2);
                    viewOne.result.setText(result.afisarePolinom());
                }
            }

            if (e.getSource() == viewOne.getInmultire()) {
                boolean ok5, ok6;
                p1 = viewOne.getPrimulPolinom();
                p2 = viewOne.getAlDoileaPolinom();
                ok5 = polinom1.validarePolinom(p1);
                ok6 = polinom2.validarePolinom(p2);
                if (ok5 == false || ok6 == false)
                    viewOne.showError("Invalid data/ Date invalide");
                else {
                    polinom1.descompunePolinom(p1);
                    polinom2.descompunePolinom(p2);
                    result.inmultirePolinoame(polinom1, polinom2);
                    viewOne.result.setText(result.afisarePolinom());
                }
            }

            if (e.getSource() == viewOne.getDerivare()) {
                boolean ok7;
                p1 = viewOne.getPrimulPolinom();
                ok7 = polinom1.validarePolinom(p1);
                if (ok7 == false)
                    viewOne.showError("Invalid data/ Date invalide");
                else {
                    polinom1.descompunePolinom(p1);
                    result.derivarePolinoame(polinom1);
                    viewOne.result.setText(result.afisarePolinom());
                }
            }

            if (e.getSource() == viewOne.getIntegrare()) {
                boolean ok8;
                String result = "";
                p1 = viewOne.getPrimulPolinom();
                ok8 = polinom1.validarePolinom(p1);
                if (ok8 == false )
                    viewOne.showError("Invalid data/ Date invalide");
                else {
                    polinom1.descompunePolinom(p1);
                    result = polinom1.integrarePolinoame(polinom1);
                    viewOne.result.setText(result);
                }
            }
            if(e.getSource() == viewOne.getReset())
            {
                viewOne.result.setText("");
                viewOne.primulPolinom.setText("");
                viewOne.alDoileaPolinom.setText("");
            }


        }
    }
}
