package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.*;

public class View extends JFrame
{

    JTextField primulPolinom = new JTextField(20);
    private JLabel polinom1 = new JLabel("First polynom/Primul polinom:");
    JTextField alDoileaPolinom = new JTextField(20);
    private JLabel polinom2 = new JLabel("Second polynom/Al doilea polinom:");

    private JButton suma = new JButton(" Addition / Adunare   ");
    private JButton scadere = new JButton(" Subtraction / Scadere");
    private JButton inmultire = new JButton("Multiplication / Inmultire");
    private JButton impartire= new JButton("  Division / Impartire        ");
    private JButton derivare = new JButton("Derivative / Derivare  ");
    private JButton integrare = new JButton("Integral / Integrare     ");

    private JButton reset = new JButton("RESET/RESETARE");

    JTextField result = new JTextField(30);
    private JLabel rezultat = new JLabel("Result/Rezultat");

    View()
    {
        JFrame window = new JFrame("Polynomial Calculator/Calculator Polinoame");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(new Dimension(750, 350));

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        JPanel p4 = new JPanel();
        JPanel p5 = new JPanel();
        JPanel p6 = new JPanel();
        JPanel p7 = new JPanel();

        p1.setLayout(new BoxLayout(p1,BoxLayout.Y_AXIS));
        polinom1.setFont(new Font("Calibri",Font.BOLD, 20));
        polinom2.setFont(new Font("Calibri",Font.BOLD, 20));
        rezultat.setFont(new Font("Calibri",Font.BOLD, 20));

        primulPolinom.setPreferredSize(new Dimension(250,50));
        alDoileaPolinom.setPreferredSize(new Dimension(250,50));
        result.setPreferredSize(new Dimension(250,50));

        primulPolinom.setBackground(new Color(204, 153, 255));
        alDoileaPolinom.setBackground(new Color(204, 153, 255));
        result.setBackground(new Color(204, 153, 255));

        primulPolinom.setFont(new Font("Calibri", Font.BOLD, 20));
        alDoileaPolinom.setFont(new Font("Calibri", Font.BOLD, 20));
        result.setFont(new Font("Calibri", Font.BOLD, 20));

        p1.add(polinom1);
        p1.add(primulPolinom);

        p1.add(polinom2);
        p1.add(alDoileaPolinom);

        p2.setLayout(new BoxLayout(p2,BoxLayout.Y_AXIS));
        p3.setLayout(new BoxLayout(p3,BoxLayout.Y_AXIS));
        p4.setLayout(new BoxLayout(p4,BoxLayout.Y_AXIS));

        suma.setPreferredSize(new Dimension(55,55));
        scadere.setPreferredSize(new Dimension(45,45));
        inmultire.setPreferredSize(new Dimension(45,45));
        impartire.setPreferredSize(new Dimension(45,45));
        derivare.setPreferredSize(new Dimension(45,45));
        integrare.setPreferredSize(new Dimension(45,45));
        reset.setPreferredSize(new Dimension(45,45));

        suma.setFont(new Font("Calibri", Font.BOLD, 20));
        scadere.setFont(new Font("Calibri", Font.BOLD, 20));
        inmultire.setFont(new Font("Calibri", Font.BOLD, 20));
        impartire.setFont(new Font("Calibri", Font.BOLD, 20));
        derivare.setFont(new Font("Calibri", Font.BOLD, 20));
        integrare.setFont(new Font("Calibri", Font.BOLD, 20));
        reset.setFont(new Font("Calibri", Font.BOLD, 20));

        suma.setBackground(new Color(166, 77, 255));
        scadere.setBackground(new Color(166, 77, 255));
        inmultire.setBackground(new Color(166, 77, 255));
        impartire.setBackground(new Color(166, 77, 255));
        derivare.setBackground(new Color(166, 77, 255));
        integrare.setBackground(new Color(166, 77, 255));
        reset.setBackground(new Color(166, 77, 255));

        suma.setForeground(Color.WHITE);
        scadere.setForeground(Color.WHITE);
        inmultire.setForeground(Color.WHITE);
        impartire.setForeground(Color.WHITE);
        derivare.setForeground(Color.WHITE);
        integrare.setForeground(Color.WHITE);
        reset.setForeground(Color.WHITE);

        p2.add(suma);
        p2.add(scadere);

        p3.add(inmultire);
        p3.add(impartire);

        p4.add(derivare);
        p4.add(integrare);

        p5.setLayout((new BoxLayout(p5, BoxLayout.X_AXIS)));
        p6.setLayout(new BoxLayout(p6, BoxLayout.Y_AXIS));
        p7.setLayout(new BoxLayout(p7, BoxLayout.Y_AXIS));

        p5.add(p2);
        p5.add(p3);
        p5.add(p4);
        p6.add(rezultat);
        p6.add(result);
        p6.add(reset);
        p7.add(p1);
        p7.add(p5);
        p7.add(p6);

        p1.setBackground(new Color(230, 204, 255));
        p2.setBackground(new Color(230, 204, 255));
        p3.setBackground(new Color(230, 204, 255));
        p4.setBackground(new Color(230, 204, 255));
        p5.setBackground(new Color(230, 204, 255));
        p6.setBackground(new Color(230, 204, 255));
        p7.setBackground(new Color(230, 204, 255));

        window.setContentPane(p7);
        window.getContentPane().setBackground(new Color(230,204,255));
        window.setVisible(true);

    }

    public String getPrimulPolinom() {
        return primulPolinom.getText();
    }

    public String getAlDoileaPolinom() {
        return alDoileaPolinom.getText();
    }

    public JButton getSuma() {
        return suma;
    }

    public JButton getScadere() {
        return scadere;
    }

    public JButton getInmultire() {
        return inmultire;
    }

    public JButton getImpartire() {
        return impartire;
    }

    public JButton getDerivare() {
        return derivare;
    }

    public JButton getIntegrare() {
        return integrare;
    }

    public JButton getReset() {
        return reset;
    }

    public void showError(String msj)
    {
        JOptionPane.showMessageDialog(this,msj);
    }

    void addPolyCalcListener ( ActionListener a)
    {
        suma.addActionListener(a);
        scadere.addActionListener(a);
        inmultire.addActionListener(a);
        impartire.addActionListener(a);
        derivare.addActionListener(a);
        integrare.addActionListener(a);
        reset.addActionListener(a);

    }

    public static void main(String[] args)
    {
        View viewOne = new View();
        Controller controllerOne = new Controller(viewOne);

    }

}

